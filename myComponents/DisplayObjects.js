/**
 * @param Objects 
 * Component to show/hide comments
 * by Zhe Xu
 * @return display the props: value of the objects
 */
import React, { Component ,PropTypes} from 'react';
import {
  Text,
  View,
} from 'react-native';
import DisplayOneObject from './DisplayOneObject.js';

class DisplayObjects extends Component {
  static propTypes ={
    objectsToDisplay: PropTypes.array.isRequired,
   // ignoreID: PropTypes.array,// not finished
    viewStyle: View.propTypes.style,
    keyStyle: Text.propTypes.style,  
  };

  static get defaultProps() {
          return {
              viewStyle:{
                backgroundColor:'#d4c4fb',
                marginTop:10,
                padding:3,
              } ,
              keyStyle: {
                color:'#000000', 
                fontWeight:'bold',
              },
          } 
  };

  render(){
    return(
      <View>
        { this._display(this.props.objectsToDisplay, this.props.viewStyle, this.props.keyStyle) }
      </View>
    );

  }

  _display(objs, viewStyle ,keyStyle){
    var viewArray = [];

    for(var i in objs){
      viewArray.push((
      <View style={viewStyle}>
        <DisplayOneObject objectToDisplay = {objs[i]} style = {keyStyle}/>
      </View>));
    }
    return viewArray;
  }

}

module.exports = DisplayObjects;