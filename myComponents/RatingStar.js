/**
 * @param null 
 * Component to show a rating star system
 * by Zhe Xu
 * props:
 *   starNo: PropTypes.number -- set the total number of stars (default: 4)
 *   litStarNo: PropTypes.number -- set how many stars are lit when initialized (default: 0)
 *   starWidth: PropTypes.number -- set width for a sigle star (default: 20)
 *   starHeight: PropTypes.number -- set height for a single star (default: 20)
 * @return a rating star component
 */
import React, { Component ,PropTypes} from 'react';
import {
  Text,
  View,
  Image,
  TouchableHighlight,
} from 'react-native';

class RatingStar extends Component {

  static propTypes ={
    starNo: PropTypes.number,
    litStarNo: PropTypes.number,
    starWidth: PropTypes.number,
    starHeight: PropTypes.number,
  };

  static get defaultProps() {
    return {
      starNo: 4,
      litStarNo: 0,
      starWidth: 20,
      starHeight: 20,
    }
  }

  constructor(){
    super();

    this.state ={
      lit: false,
      star: [],
      lit_stat_No: 0,
    };   

  }
  
  componentWillMount(){
    this.setState({
      star:this._lightStar(this.props.lit_stat_No),
      lit_stat_No: this.props.litStatNo,
    });
  }

  render(){

    return(
      <View style={{flex:1 ,flexDirection: 'row',}}>
        {this.state.star}
      </View>
    );
     
  
  }

  _lightStar(stars_to_be_lit = 0){
    let star_show = [];

    let ii =0;
    while(ii<this.props.starNo){
      star_show.push(this.empty_star(ii));
      ii++;
    }

    ii=0;
    while(ii<stars_to_be_lit){
      star_show[ii]=this.lit_star(ii);
      ii++;
    }


    return star_show;
  }

  empty_star(index){
      return this._displayTouchableImage(index,require('../myImage/Rating_star/rating_star_empty.png'));
  }
  
  lit_star(index){
     return this._displayTouchableImage(index,require('../myImage/Rating_star/rating_star_lit.png'));
  }

 _displayTouchableImage(index,img_input){
   return(
      <TouchableHighlight
        index = {index}
        //ref = {(ref)=>{this.tag[index] = ref}}
        //onPress={this._onPress.bind(this)}
        onPress={()=>this._onPress(index)}
        underlayColor= '#fff'
        style ={{height:this.props.starHeight, width:this.props.starWidth,}}
      >
        <Image style = {{height:this.props.starHeight, width:this.props.starWidth}} source={img_input} />
      </TouchableHighlight>
    );
 }

  _onPress(ref){
    //if not press on the first star
    if(ref>0){
      // only run when this is a different press 
      if(ref+1 != this.state.lit_stat_No){
        this._setLitStarNo(ref+1);
        console.warn("set state for lit stars");
      }
    }
    // if this is a press on the first star
    else{
      //if there are already lit star(s)
      if(this.state.lit_stat_No>0){
        this._setLitStarNo(0);
        console.warn("set all star empty!!");
      }
      //if no lit star
      else{
       this._setLitStarNo(1);
      }
    }
    //console.warn('press star '+ref);
  }

  _setLitStarNo(number){
    this.setState({
      star:this._lightStar(number),
      lit_stat_No: number,
    });
  }


}

module.exports = RatingStar;