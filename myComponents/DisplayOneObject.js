/**
 * @param oneObject 
 * Component to show/hide comments
 * by Zhe Xu
 * @return display the props: value of the objects
 */
import React, { Component ,PropTypes} from 'react';
import {
  Text,
  View,
} from 'react-native';
//import stylePropType from 'react-style-proptype';

class DisplayOneObject extends Component {
  static propTypes ={
    objectToDisplay: PropTypes.object.isRequired,
   // ignoreID: PropTypes.array,// not finished
    keyStyle: Text.propTypes.style,
  };

 static get defaultProps() {
  return {
    keyStyle: {
      color:'#000000', 
      fontWeight:'bold',
    },
  } 
};

  render(){
   return(
     <View>
      { this._display(this.props.objectToDisplay, this.props.keyStyle) }
    </View>
   );
    
  }

_display(obj, keyStyle){
    var textArray =[];
    for(var props in obj){
        if(props!='id'&&props!='postId')
          textArray.push((<Text> <Text style={keyStyle}>{props}: </Text>{obj[props]} </Text>));
      }
    return textArray;
  } 

}

module.exports = DisplayOneObject;