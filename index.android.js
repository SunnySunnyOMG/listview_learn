/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
} from 'react-native';
//import  Accordion from 'react-native-accordion';
import DisplayComment from './myComponents/DisplayComment.js';
import DisplayOneObject from './myComponents/DisplayOneObject.js';
import DisplayObjects from './myComponents/DisplayObjects.js';
import RatingStar from './myComponents/RatingStar.js';

export default class ListView_Training extends Component {

// 构造函数声明所需的变量; 获取所需的数据
 constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1!==r2});
    var commentIndex =[];

    this.state = { dataSource: ds.cloneWithRows([]) , 
                   postJson:[], 
                   commentJson: [],
                   comment:['empty!!!!'],
                   loadingDone: false,
                };              
  }

  componentWillMount(){
    this.fetchSomething('https://jsonplaceholder.typicode.com/comments');  
    this.fetchSomething('https://jsonplaceholder.typicode.com/posts', 'listView');
  }

  render() {

    return (
      <ListView 
        dataSource={this.state.dataSource}
        renderRow={(rowData) => {

          return(
          <View style={styles.container}>
            <View>
              <Image source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}} style={styles.avatar}>
                <Text style ={styles.id}>userId: {rowData.userId} </Text>
              </Image>

              <RatingStar />
              
            </View>

            <View style = {styles.infoBox}> 
              <DisplayOneObject objectToDisplay = {rowData} keyStyle = {styles.key} />        
              {this.renderComment(this.getComments(rowData.id))}
            </View>

          </View>
          )
          }
        }
      />
    );
  }

  fetchSomething(url, oprator = '',method='GET'){
    fetch(url, {method:method,})
      .then( (response) => response.json())// convert to Json, return a promise
      .then((responseJ)=>{
     if (oprator == 'listView'){
       if (this.state.loadingDone){
         this.listData = responseJ;
       }else
       {
         this.listData = [{'Status':'Now Loading...'}];
       }

       this.setState({
         //save the posts array in a state 'postJson' 
         postJson: responseJ,
         dataSource: this.state.dataSource.cloneWithRows(this.listData),
       });
      }    
     else{
       /**
        * 试了几种方法，觉得现在这种解决方案是最直接的，即：
        *    当comment数据读取完毕后才载入 listview 
        *    在读取时对用户显示： status：loading...
        * NOTE: 不是最优解，但是是最直接的
        **/
        this.buildIndexComment(responseJ);
        
        this.setState( { 
        commentJson: responseJ ,
        loadingDone: true, 
        dataSource: this.state.dataSource.cloneWithRows(this.state.postJson),
      });
      console.warn('comments loading done!')
       //console.warn(JSON.stringify(this.state.commentJson));
     }
     })
      .catch((error) => {
          console.error(error);
        }); 
  }

   getComments(postID){
    if(postID)
      return this.commentHash[postID.toString()];
    else
      return [{'status':' Error! Cannot find a valid postId!'}];
   }

   buildIndexComment(comments){
     this.commentHash = {};
     comments.forEach(comment => {
       let postId = comment.postId.toString();
       if (this.commentHash[postId]){
         this.commentHash[postId].push(comment);
       }
       else{
         this.commentHash[postId] = [];
         this.commentHash[postId].push(comment);
       }
     })
   }

   renderComment(objs){
      //dont need to render comments if not loaded yet
      if (!this.state.loadingDone)
        return;
      var header = (
      <View>
        <Text style={styles.buttonText} >COMMENTS</Text>
      </View>
      );
      
      var content = (
      <View>
        <DisplayObjects objectsToDisplay={objs}/>
      </View>
      );     

       return (
        <DisplayComment
          underlayColor = '#fff'   
          header={header}
          content={content}
          //onPress = {this._onPress}
        />
      );
  }

  _onPress(){
    //could define more operation here...
    console.warn('onPress operation'); 
  }
}


//加了一些样式 =。=
const styles = StyleSheet.create({
  list:{
    flex:1
  },
  container: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  infoBox:{
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  id:{
     color: '#ff1493',
     fontWeight:'bold',
  },
  key:{
    color:'#000000',
    fontWeight:'bold',
  },
  avatar:{
    marginRight:10,
    height:80,
    width:80,
  },
  buttonText:{
    padding:5,
    backgroundColor: '#0693e3',
    alignItems: 'flex-start',
    color:'#fff',
    fontWeight:'bold'
  },
  comment:{
    backgroundColor:'#d4c4fb',
    marginTop:10,
    padding:3,
  }
});

AppRegistry.registerComponent('ListView_Training', () => ListView_Training);
