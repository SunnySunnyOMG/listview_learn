/**
 * Component to show/hide comments
 * by Zhe Xu
 */
import React, { Component ,PropTypes} from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
} from 'react-native';

class DisplayComment extends Component {
  static propTypes ={
    header: PropTypes.element.isRequired,
    content: PropTypes.element.isRequired,
    onPress: PropTypes.func,
    underlayColor: PropTypes.string,
  };

  constructor(){
    super();
    this.state = {
      is_visible: false,
      comment_current_height: 0,
      comment_actual_height: 0 ,
    };
  }

  render() 
{        
    return (            
     <View style = {styles.container}>

       <TouchableHighlight
         underlayColor={this.props.underlayColor}
         onPress ={()=>{this.onPress()}}
       >
        {this.props.header}
      </TouchableHighlight>

       <View style ={{height: this.state.comment_current_height}} ref={(ref) => { this.commentTag = ref; }} >
          {this.props.content}
       </View>

     </View>
    );    
 }

  componentDidMount() {
    this.contentHeight();
  }

 onPress(){
   //console.warn('Press COMMENTS');
   this.toggle();
 }

 toggle(){
   // toggle the visibility of comments
   this.setState({
      is_visible: !this.state.is_visible,
      comment_current_height: !this.state.is_visible ? this.state.comment_actual_height : 0,       
    });
 }

 contentHeight(){
   //get content height
    if (this.commentTag) {
      this.commentTag.measure((ox, oy, width, height, px, py) => {
        // Sets content height in state
        this.setState({
          comment_current_height: 0,
          comment_actual_height: height,
        });
      }); 
      //console.warn('comment_actual_height:' + this.state.comment_actual_height);
    }
 }

}

//style sheet
const styles = StyleSheet.create({
  container:{
    overflow:'hidden',

  },
});

module.exports = DisplayComment;